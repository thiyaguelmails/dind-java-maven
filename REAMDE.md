# dind-java-maven

Docker image that combines
[maven](https://hub.docker.com/_/maven/)
and
[docker-in-docker](https://hub.docker.com/_/docker/).

## Building

`docker build -t <tag>`

## Maintenence

This has to be kept up to date with the other two images manually.
